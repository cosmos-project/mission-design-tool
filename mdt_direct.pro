################################################################################
# Qt project file for COSMOS Data Management Tool (DMT)
#
# Copyright (C) 2015 Hawaii Space Flight Laboratory
# cosmos.project@hsfl.hawaii.edu
#
################################################################################

message("----------------------------------------------------------")
message("Mission Design Tool (MDT)")

QT      += core gui opengl widgets printsupport
CONFIG  += c++11
TARGET   = MDT
TEMPLATE = app

COSMOS           = $$PWD/../..
CORE             = $$COSMOS/core
TOOLS            = $$COSMOS/tools
MDT              = $$TOOLS/mdt

CORE_LIBRARIES   = $$CORE/libraries
CORE_THIRDPARTY  = $$CORE/libraries/thirdparty
CORE_SUPPORT     = $$CORE/libraries/support
CORE_ZLIB        = $$CORE_THIRDPARTY/zlib
CORE_DIRENT      = $$CORE_THIRDPARTY/dirent
TOOLS_QTSUPPORT  = $$TOOLS/libraries


INCLUDEPATH += $$PWD
INCLUDEPATH += $$CORE
INCLUDEPATH += $$CORE_LIBRARIES
INCLUDEPATH += $$TOOLS_QTSUPPORT
INCLUDEPATH += $$CORE_THIRDPARTY
INCLUDEPATH += $$CORE_ZLIB
INCLUDEPATH += $$PWD/../libraries
INCLUDEPATH += $$PWD/../../thirdparty

#QMAKE_LIBDIR += $$COSMOSCORE/lib
#LIBS += -lCosmosAgent -lCosmosDeviceCpu -lCosmosPhysics -lCosmosSupport -lCosmosMath -llocalzlib

win32 {

    # for MinWG
    *-g++* {
        message( "Compiler: MinGW" )
        LIBS += -lpthread -lwsock32 -lwinmm -lws2_32 -liphlpapi
    }

    # for MSVC
    *-msvc* {
        message( "Compiler: MSVC" )

        #MSVC does not have pthread
        LIBS += -lwsock32 -lwinmm -lws2_32 -liphlpapi

        #contains(MODULES, DIRENT){
        INCLUDEPATH += $$COSMOS/core/libraries/thirdparty/dirent
        SOURCES     += $$COSMOS/core/libraries/thirdparty/dirent/dirent.c
        HEADERS     += $$COSMOS/core/libraries/thirdparty/dirent/dirent.h

        DEFINES += NOMINMAX
    }
}

unix:!macx {
    LIBS += -lGL -lGLU
}

SOURCES += \
    $$PWD/*.cpp \
    $$CORE_LIBRARIES/support/*.cpp \
    $$CORE_LIBRARIES/agent/*.cpp \
    $$CORE_LIBRARIES/math/*.cpp \
    $$CORE_LIBRARIES/physics/*.cpp \
    $$CORE_LIBRARIES/device/cpu/*.cpp \
    $$CORE_ZLIB/*.c \
    $$PWD/../libraries/cosmoswidget/*.cpp \
    $$PWD/../libraries/qcustomplot/*.cpp

HEADERS  += \
    $$PWD/*.h \
    $$CORE_LIBRARIES/support/*.h \
    $$CORE_LIBRARIES/agent/*.h \
    $$CORE_LIBRARIES/math/*.h \
    $$CORE_LIBRARIES/physics/*.h \
    $$CORE_LIBRARIES/device/cpu/*.h \
    $$CORE_ZLIB/*.h \
    $$PWD/../libraries/cosmoswidget/*.h \
    $$PWD/../libraries/qcustomplot/*.h

static { # everything below takes effect with CONFIG += static
    CONFIG += static
    CONFIG += staticlib # this is needed if you create a static library, not a static executable
    DEFINES += STATIC
    message("~~~ static build ~~~") # this is for information, that the static build is done
    mac: TARGET = $$join(TARGET,,,_static) #this adds an _static in the end, so you can seperate static build from non static build
    win32: TARGET = $$join(TARGET,,,s) #this adds an s in the end, so you can seperate static build from non static build
}

# change the nama of the binary, if it is build in debug mode
CONFIG(debug, debug|release) {
    message("~~~ debug build ~~~")     mac: TARGET = $$join(TARGET,,,_debug)
     win32: TARGET = $$join(TARGET,,,d)
}
