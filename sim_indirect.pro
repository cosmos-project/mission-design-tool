################################################################################
# Qt project file for COSMOS Simulator (SIM)
#
# Copyright (C) 2015 Hawaii Space Flight Laboratory
# cosmos.project@hsfl.hawaii.edu
#
################################################################################

message("----------------------------------------------------------")
message("Mission Design Tool (MDT)")

QT      += core gui opengl widgets printsupport
CONFIG  += c++11
TARGET   = SIM
TEMPLATE = app

COSMOS = $$(COSMOS)
isEmpty(COSMOS) {
    win32 {
        COSMOS = c:/cosmos
        COSMOSCORE = $$COSMOS
    }
    unix {
        macx {
            COSMOS = /cosmos
            COSMOSCORE = $$COSMOS
        !macx {
            COSMOS = /usr/local/cosmos
            COSMOSCORE = $$COSMOS
        }
    }
message("COSMOS Not Set, Setting to $$COSMOS")
} else {
message("COSMOS Set to $$COSMOS")
    COSMOS = $(COSMOS)
    win32 {
        COSMOSCORE = $$COSMOS
    }
    unix {
        macx {
            COSMOSCORE = $$COSMOS
        }
        !macx {
            COSMOSCORE = $$COSMOS
            message(COSMOSCORE set to $$COSMOSCORE)
        }
    }
}

TOOLS_QTSUPPORT  = $$PWD/libraries
SRC = $$PWD

INCLUDEPATH += $$SRC
INCLUDEPATH += $$COSMOSCORE/include
INCLUDEPATH += $$PWD/../libraries
INCLUDEPATH += $$PWD/../../thirdparty

QMAKE_LIBDIR += $$COSMOSCORE/lib
LIBS += -lCosmosAgent -lCosmosDeviceCpu -lCosmosPhysics -lCosmosSupport -lCosmosMath -llocalzlib

win32 {

    # for MinWG
    *-g++* {
        message( "Compiler: MinGW" )
        LIBS += -lpthread -lwsock32 -lwinmm -lws2_32 -liphlpapi  -lglut32  -lglu32  -lopengl32
    }

    # for MSVC
    *-msvc* {
        message( "Compiler: MSVC" )

        #MSVC does not have pthread
        LIBS += -lwsock32 -lwinmm -lws2_32 -liphlpapi -ldirent

        DEFINES += NOMINMAX
    }
}

unix:!macx {
    LIBS += -lGL -lGLU
}

SOURCES += \
    $$SRC/*.cpp \
    $$PWD/../libraries/cosmoswidget/*.cpp \
    $$PWD/../../thirdparty/qcustomplot/*.cpp

HEADERS  += \
    $$SRC/*.h \
    $$PWD/../libraries/cosmoswidget/*.h \
    $$PWD/../../thirdparty/qcustomplot/*.h

# change the name of the binary, if it is build in debug mode
CONFIG(debug, debug|release) {
    message("~~~ debug build ~~~")     mac: TARGET = $$join(TARGET,,,_debug)
     win32: TARGET = $$join(TARGET,,,d)
}
}
