# COSMOS Mission Design Tool (MDT) #

The COSMOS MDT will simulate your satellite orbit and help you to understand the power generated on orbit, the ground station contacts and other common operation aspects of your missions. 

[![Build status](https://ci.appveyor.com/api/projects/status/coxnjge9regohwwy?svg=true)](https://ci.appveyor.com/project/spacemig/mission-design-tool)

### How do I get set up? ###

* Dependencies

- cosmos/core
- cosmos/toosl/libraries

### Who do I talk to? ###

* Owner: pilger@hsfl.hawaii.edu