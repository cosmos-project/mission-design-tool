#include "support/configCosmos.h"

#include <QApplication>

#include "mainwindow.h"

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);

    MainWindow window;
    window.move(0,0);
    window.showNormal();
    window.show();

    return a.exec();
}
