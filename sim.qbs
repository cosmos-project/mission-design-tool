import qbs.Environment

Project {
    name: "Satellite Simulation Tool"

    references: [
        "../../tools/libraries/cosmoswidget/cosmoswidget.qbs",
        "../../thirdparty/qcustomplot/qcustomplot.qbs",
        "../../thirdparty/glm/glm.qbs",
    ]

    Product {
        name: "sim"
        type: "application"
        files: ["*.cpp", "*.h"]
        Depends { name: "cpp" }
        Properties {
            condition: qbs.targetOS.contains("windows")
            cpp.minimumWindowsVersion: "7.0"
        }
        Properties {
            condition: qbs.targetOS.contains("windows") && qbs.toolchain.contains("mingw")
            cpp.dynamicLibraries: ["pthread", "wsock32", "winmm", "ws2_32", "iphlpapi"]
        }
        cpp.cxxLanguageVersion : "c++11"
        cpp.commonCompilerFlags : "-std=c++11"
        property string Cosmos: Environment.getEnv("COSMOS")
        cpp.includePaths : [Cosmos+"/include"]
        cpp.libraryPaths : [Cosmos+"/lib"]
        cpp.staticLibraries : ["CosmosAgent","CosmosDeviceCpu", "CosmosPhysics", "CosmosSupport", "CosmosMath", "zlib"]


        Depends {
            name: "Qt";
            submodules: ["core", "gui", "widgets", "opengl", "printsupport"]
        }


        Depends {
            name: ["qcustomplot"]
        }

        Depends {
            name: ["cosmoswidget"]
        }

    }
}
