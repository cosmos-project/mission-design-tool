#include "mainwindow.h"
#include <thread>

int32_t cthreadloopstate=0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    //    int32_t iretn;
    size_t index;
	timeInfoStack.stack = new vector<cosmosstruc>;

    // Timer stuff
    timeDataTimer = new QTimer(this);
    connect(timeDataTimer, SIGNAL(timeout()), this, SLOT(timeDataUpdate()));


    // ---------------------------------------
    QWidget *window = new QWidget;

    QVBoxLayout *verticalLayout = new QVBoxLayout;


    // Control widgets
    QGridLayout *controlLayout = new QGridLayout;
    verticalLayout->addLayout(controlLayout);

    controlLayout->setContentsMargins (5, 5, 5, 5);

    m_comboSelectNode = new QComboBox;
    vector < string > nodes = data_list_nodes();
    for (size_t i=0; i<nodes.size(); ++i)
    {
        cosmosstruc *cinfo = json_create();
        json_setup_node(nodes[i], cinfo);
        cinfos.push_back(cinfo);
        m_comboSelectNode->addItem(QString::fromStdString(nodes[i]));
    }
    connect(m_comboSelectNode, SIGNAL(activated(int)), this, SLOT(comboSelectNode(int)));
    controlLayout->addWidget(m_comboSelectNode, 0, 0);

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("Go To &End");
        connect(twidget, &CosmosPushButton::pressed, [&]()
        {
			if ((*timeInfoStack.stack).size())
            {
				timeDataIter = (*timeInfoStack.stack).size() - 1;
                timeDataIterHold = timeDataIter;
                timeDataDirection = 0;
				timeDataValue = (*timeInfoStack.stack)[timeDataIter].node.utc;
            }
        });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 0);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("Go To &Start");
        connect(twidget, &CosmosPushButton::pressed, [&]() { buttonGoToStart(); });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 1);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("&Pause");
        connect(twidget, &CosmosPushButton::pressed, [&]()
        {
            timeDataIterHold = timeDataIter;
            timeDataDirection = 0;
        });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 2);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("Step Backward");
        connect(twidget, &CosmosPushButton::pressed, [&]()
        {
            timeDataIterHold = timeDataIter - 1;
            timeDataDirection = -1;
        });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 3);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("Step Forward");
        connect(twidget, &CosmosPushButton::pressed, [&]()
        {
            timeDataIterHold = timeDataIter + 1;
            timeDataDirection = 1;
        });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 4);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("Play &Forward");
        connect(twidget, &CosmosPushButton::pressed, [&]()
        {
			if ((*timeInfoStack.stack).size())
            {
				timeDataIterHold = (*timeInfoStack.stack).size() - 1;
                timeDataDirection = 1;
            }
        });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 5);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("Play &Backward");
        connect(twidget, &CosmosPushButton::pressed, [&]()
        {
            timeDataIterHold = 0;
            timeDataDirection = -1;
        });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 6);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PUSHBUTTON, "")) < widgetHandler.size())
    {
        CosmosPushButton *twidget = ((CosmosPushButton *)widgetHandler.getWidget(index));
        twidget->setText("Export Plots");
        twidget->setToolTip("Export plot data to a .txt file");
        connect(twidget, &CosmosPushButton::clicked, [&]()
        {
            exportPlots();
        });

        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 7);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::SLIDER, "", "Time Step")) < widgetHandler.size())
    {
        CosmosSlider *twidget = ((CosmosSlider *)widgetHandler.getWidget(index));
        twidget->setRange(-1, -1);
        twidget->setOrientation(Qt::Horizontal);
        connect(twidget, &CosmosSlider::valueChanged, [&, twidget]()
        {
            if ((size_t)twidget->value() != timeDataIter)
            {
				timeDataValue = (*timeInfoStack.stack)[twidget->value()].node.utc;
                timeDataIter = twidget->value();
            }
        });
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 2, 1, 1, 6);
        m_sliderIter = (CosmosSlider *)widgetHandler.getWidget(index);
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::SPINBOX, "", "Time Step")) < widgetHandler.size())
    {
        CosmosSpinBox *twidget = ((CosmosSpinBox *)widgetHandler.getWidget(index));
        ((CosmosSpinBox *)widgetHandler.getWidget(index))->setMaximum(INT_MAX);
        controlLayout->addLayout(widgetHandler.getWidgetLayout(index), 2, 0);
        twidget->setMaximumWidth(300);
        dataiteration = (CosmosSpinBox *)widgetHandler.getWidget(index);
    }

    //graph grid

    QGridLayout *graphsLayout = new  QGridLayout;
    verticalLayout->addLayout(graphsLayout);
    //graphsLayout->setHorizontalSpacing();
    graphsLayout->setContentsMargins (5, 5, 5, 5);


    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PLOT, "node_loc_pos_eci_s", "", true)) < widgetHandler.size())
    {
        ((CosmosPlot *)widgetHandler.getWidget(index))->setMinimumHeight(150);
        ((CosmosPlot *)widgetHandler.getWidget(index))->setMinimumWidth(560);
//        ((CosmosPlot *)widgetHandler.getWidget(index))->setMaximumHeight(150);
//        ((CosmosPlot *)widgetHandler.getWidget(index))->setMaximumWidth(560);
        graphsLayout->addLayout(widgetHandler.getWidgetLayout(index), 0, 0);
        connect(this, SIGNAL(plotVis(bool)), ((CosmosPlot *)widgetHandler.getWidget(index)), SLOT(setVisible(bool)));
        connect(this, SIGNAL(plotVis(bool)), (widgetHandler.getWidgetCombo(index)), SLOT(setVisible(bool)));
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PLOT, "node_loc_pos_eci_v", "", true)) < widgetHandler.size())
    {
        ((CosmosPlot *)widgetHandler.getWidget(index))->setMinimumHeight(150);
       ((CosmosPlot *)widgetHandler.getWidget(index))->setMinimumWidth(560);
        graphsLayout->addLayout(widgetHandler.getWidgetLayout(index), 0, 1);
        connect(this, SIGNAL(plotVis(bool)), ((CosmosPlot *)widgetHandler.getWidget(index)), SLOT(setVisible(bool)));
        connect(this, SIGNAL(plotVis(bool)), (widgetHandler.getWidgetCombo(index)), SLOT(setVisible(bool)));
    }

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PLOT, "node_loc_att_icrf_s", "", true)) < widgetHandler.size())
    {
        ((CosmosPlot *)widgetHandler.getWidget(index))->setMinimumHeight(150);
        ((CosmosPlot *)widgetHandler.getWidget(index))->setMinimumWidth(560);
        graphsLayout->addLayout(widgetHandler.getWidgetLayout(index), 0, 2);
        connect(this, SIGNAL(plotVis(bool)), ((CosmosPlot *)widgetHandler.getWidget(index)), SLOT(setVisible(bool)));
        connect(this, SIGNAL(plotVis(bool)), (widgetHandler.getWidgetCombo(index)), SLOT(setVisible(bool)));
    }




    // Simulator controls
    QGridLayout *simulatorLayout = new QGridLayout;
    verticalLayout->addLayout(simulatorLayout);
    simulatorLayout->setContentsMargins (5, 5, 5, 5);


    simulatorLayout->setHorizontalSpacing(28);


   if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::ELEMENTS, "Elements", "Elements")) < widgetHandler.size())
   {
       CosmosElements* twidget = ((CosmosElements* )widgetHandler.getWidget(index));
       twidget->setkep(&kep);
       twidget->seteci(&eci);
       twidget->addTabs();
       twidget->setMaximumWidth(150);
       connect(twidget, &CosmosElements::updatedOrbit, [&]() { buttonUpdateKepOrbit(); });
       connect(twidget, &CosmosElements::exportKep, [&]() { exportkep(); });
       connect(twidget, &CosmosElements::exportEci, [&]() { exporteci(); });
    //   twidget->setAgent(&cagent);
       simulatorLayout->addLayout(widgetHandler.getWidgetLayout(index), 0, 0, 3, 1);
    }

    // Data display widgets

    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::MAP, "mapping/earth/earth.jpg")) < widgetHandler.size())
    {
        ((CosmosMap *)widgetHandler.getWidget(index))->setMinimumHeight(435);
        ((CosmosMap *)widgetHandler.getWidget(index))->setMaximumHeight(1000);
       ((CosmosMap *)widgetHandler.getWidget(index))->setMinimumWidth(400);
//        ((CosmosMap *)widgetHandler.getWidget(index))->setMaximumWidth(1300);
        simulatorLayout->addLayout(widgetHandler.getWidgetLayout(index), 0, 1, 8, 1);
    }

    QGridLayout *outputsLayout = new  QGridLayout;
    verticalLayout->addLayout(outputsLayout);
    outputsLayout->setHorizontalSpacing(15);
    outputsLayout->setContentsMargins (5, 10, 5, 5);



    // Position widget
    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::POSITION, "", "Position")) < widgetHandler.size())
    {
        ((CosmosPosition *)widgetHandler.getWidget(index))->setMinimumHeight(150);
        ((CosmosPosition *)widgetHandler.getWidget(index))->setMinimumWidth(900);
//        ((CosmosPosition*)widgetHandler.getWidget(index))->setMaximumWidth(900);
        //simulatorLayout->addLayout(widgetHandler.getWidgetLayout(index), 8, 0, 1, 3);
        outputsLayout->addLayout(widgetHandler.getWidgetLayout(index), 0, 0);
        connect(this, SIGNAL(posVis(bool)), ((CosmosPlot *)widgetHandler.getWidget(index)), SLOT(setVisible(bool)));
        connect(this, SIGNAL(posVis(bool)), ((CosmosPlot *)widgetHandler.getWidgetLabel(index)), SLOT(setVisible(bool)));

    }


    if ((index=widgetHandler.newWidget(CosmosWidgetHandler::widgetType::PAYLOADS, "", "Payloads")) < widgetHandler.size())
    {
//        outputsLayout->addLayout(widgetHandler.getWidgetLayout(index), 7, 3, 2, 3);
        outputsLayout->addLayout(widgetHandler.getWidgetLayout(index), 1, 0);
        ((CosmosPayloads*)widgetHandler.getWidget(index))->setMaximumHeight(150);
        ((CosmosPayloads*)widgetHandler.getWidget(index))->setMinimumWidth(800);
        connect(this, SIGNAL(payVis(bool)), ((CosmosPlot *)widgetHandler.getWidget(index)), SLOT(setVisible(bool)));
        connect(this, SIGNAL(payVis(bool)), ((CosmosPlot *)widgetHandler.getWidgetLabel(index)), SLOT(setVisible(bool)));


    }

    //creating menus
    createActions();
    createMenus();

    // Some final preparations
    if (nodes.size())
    {
        comboSelectNode(0);
    }

    // Whole window
    window->setLayout(verticalLayout);

    this->setCentralWidget(window);

    timeDataEt.reset();
    timeDataBase = currentmjd();
    timeDataOffset = 0.;
    timeDataTimer->start(100);
}


void MainWindow::timeDataUpdate()
{
    if (timeDataIter != timeDataIterHold)
    {
        if (timeDataDirection)
        {
            double stepstay, stepmore, stepless;
            double timeStep = timeDataEt.lap() * timeDataSpeed * timeDataDirection / 86400.;
            timeDataValue += timeStep;
            do
            {
				stepstay = fabs((*timeInfoStack.stack)[timeDataIter].node.utc - timeDataValue);
                stepless = stepstay;
                stepmore = stepstay;
                if (timeDataIter > 0)
                {
					stepless = fabs((*timeInfoStack.stack)[timeDataIter-1].node.utc - timeDataValue);
                }
                if (stepless < stepstay)
                {
                    --timeDataIter;
                }
                else
                {
					if (timeDataIter < (*timeInfoStack.stack).size() - 1)
                    {
						stepmore = fabs((*timeInfoStack.stack)[timeDataIter+1].node.utc - timeDataValue);
                        if (stepmore < stepstay)
                        {
                            ++timeDataIter;
                        }
                    }
                }
			} while (timeDataIter != 0 && timeDataIter != (*timeInfoStack.stack).size() - 1 && (stepmore < stepstay || stepless < stepstay));
			if (timeInfoPtr != &(*timeInfoStack.stack)[timeDataIter])
            {
				timeInfoPtr = &(*timeInfoStack.stack)[timeDataIter];
                widgetHandler.setCdataIndex(timeDataIter);
            }
            cagent.post(Agent::AgentMessage::SOH, sohstring);
        }
    }

    if ((size_t)m_sliderIter->value() != timeDataIter)
    {
        m_sliderIter->setValue(timeDataIter);
    }
    dataiteration->setValue(timeDataIter);
    widgetHandler.updateCdata();
    timeDataTimer->start(100);
}


void MainWindow::comboSelectNode(int item)
{
    //    int32_t iretn;
    // Clean house
	if (timeInfoStack.stack)
    {
		(*timeInfoStack.stack).clear();
    }
    else
    {
		timeInfoStack.stack = new vector<cosmosstruc>;
    }
    timeDataIter = SIZE_MAX;
    timeDataIterHold = SIZE_MAX;
    timeDataSpeed = 1.;
    timeDataDirection = 0;
    timeDataBase = currentmjd();
    timeDataEt.reset();

    // Get node
    cagent.cinfo = cinfos[item];
	json_of_soh(sohstring, cagent.cinfo);
	eci2kep(cagent.cinfo->node.loc.pos.eci, kep);
	timeDataOffset = cagent.cinfo->node.utc - timeDataBase;
	timeInfoPtr = cagent.cinfo;
    buttonUpdateKepOrbit();
}

void MainWindow::buttonGoToStart()
{
	if ((*timeInfoStack.stack).size())
    {
        timeDataIter = 0;
        timeDataIterHold = timeDataIter;
        timeDataDirection = 0;
		timeDataValue = (*timeInfoStack.stack)[timeDataIter].node.utc;
    }
}

void MainWindow::sliderIter(int newDataIter)
{
    if ((size_t)newDataIter != timeDataIter)
    {
		timeDataValue = (*timeInfoStack.stack)[newDataIter].node.utc;
        timeDataIter = newDataIter;
    }
}

void MainWindow::buttonUpdateKepOrbit()
{
    uint32_t order = 6;
    int32_t mode = 0;
    int32_t iretn;

    // Determine starting position and attitude
//<<<<<<< .merge_file_a09012
	loc_clear(&cagent.cinfo->node.loc);
	kep2eci(kep, cagent.cinfo->node.loc.pos.eci);
	++cagent.cinfo->node.loc.pos.eci.pass;
	iretn = pos_eci(&cagent.cinfo->node.loc);
    timeDataBase = currentmjd();
	timeDataOffset = cagent.cinfo->node.utc - timeDataBase;
	cagent.cinfo->node.utcoffset = timeDataOffset;
	double mjdnow = currentmjd(cagent.cinfo->node.utcoffset);
	eci = cagent.cinfo->node.loc.pos.eci;
//=======
//    cagent.cinfo->node.utcoffset = timeDataOffset;
//    double mjdnow = currentmjd(cagent.cinfo->node.utcoffset);
//    kep.ea = 0.;
//    kep.utc = mjdnow;
//    locstruc iloc;
//    loc_clear(&iloc);
//    kep2eci(kep, iloc.pos.eci);
//    eci = iloc.pos.eci;
//    ++iloc.pos.eci.pass;
//    iretn = pos_eci(&iloc);
//>>>>>>> .merge_file_a01680
    if (iretn < 0)
    {
        printf("pos_eci: %s\n", cosmos_error_string(iretn).c_str());
    }

	cagent.cinfo->node.loc.att.lvlh.s = q_eye();
	cagent.cinfo->node.loc.att.lvlh.v = rv_zero();
	cagent.cinfo->node.loc.att.lvlh.a = rv_zero();
	++cagent.cinfo->node.loc.att.lvlh.pass;
	att_lvlh(&cagent.cinfo->node.loc);

    // Initialize hardware at that location
//<<<<<<< .merge_file_a09012
	hardware_init_eci(cagent.cinfo->devspec, cagent.cinfo->node.loc);
//=======
//    hardware_init_eci(cagent.cinfo->devspec, iloc);
//>>>>>>> .merge_file_a01680

    gj_handle gjh;
    double dt = kep.period / 1000.;
    // Initialize propagator
//<<<<<<< .merge_file_a09012
	gauss_jackson_init_eci(gjh, order, mode, dt, cagent.cinfo->node.loc.utc, cagent.cinfo->node.loc.pos.eci, cagent.cinfo->node.loc.att.icrf, cagent.cinfo->physics, cagent.cinfo->node.loc);
//    simulate_hardware(cagent.cinfo->pdata, cagent.cinfo->node.loc);
//=======
//    gauss_jackson_init_eci(gjh, order, mode, dt, iloc.utc, iloc.pos.eci, iloc.att.icrf, cagent.cinfo->physics, cagent.cinfo->node.loc);
//>>>>>>> .merge_file_a01680
    // Bring propagator forward
    vector <locstruc> locvec = gauss_jackson_propagate(gjh, cagent.cinfo->physics, cagent.cinfo->node.loc, currentmjd(cagent.cinfo->node.utcoffset));
    //Bring hardware forward
    setPayloadsState(cagent.cinfo);
    simulate_hardware(cagent.cinfo, locvec);
//    pos_clear(iloc);
//    iloc.pos.eci = cagent.cinfo->node.loc.pos.eci;
//    iloc.att.icrf = cagent.cinfo->node.loc.att.icrf;
//    iloc.utc = cagent.cinfo->node.loc.pos.eci.utc;
//    hardware_init_eci(cagent.cinfo->devspec, iloc);
//    gauss_jackson_init_eci(gjh, order, mode, dt, iloc.utc ,iloc.pos.eci, iloc.att.icrf, cagent.cinfo->physics, cagent.cinfo->node.loc);
    mjdnow = cagent.cinfo->node.loc.utc;
    cagent.cinfo->node.utc = cagent.cinfo->node.loc.utc;

    cagent.cinfo->timestamp = currentmjd();

	(*timeInfoStack.stack).clear();
	(*timeInfoStack.stack).push_back(*cagent.cinfo);

    for (size_t i=0; i<4000; ++i)
    {
        mjdnow += (dt) / 86400.;
        locvec = gauss_jackson_propagate(gjh, cagent.cinfo->physics, cagent.cinfo->node.loc, mjdnow);
        setPayloadsState(cagent.cinfo);
        simulate_hardware(cagent.cinfo, locvec);
        if (cagent.cinfo->devspec.pload_cnt && cagent.cinfo->devspec.pload[0]->flag)
        {
            printf("%d:%f ", i, cagent.cinfo->devspec.pload[0]->power);
        }
        cagent.cinfo->node.loc = locvec[locvec.size()-1];
        cagent.cinfo->node.utc = cagent.cinfo->node.loc.utc;

        cagent.cinfo->timestamp = currentmjd();
        (*timeInfoStack.stack).push_back(*cagent.cinfo);
    }
	m_sliderIter->setRange(0, (*timeInfoStack.stack).size());

	timeInfoStack.timestamp = currentmjd();
    widgetHandler.setCinfoStack(timeInfoStack);
    buttonGoToStart();
}

// void MainWindow::buttonUpdateEciOrbit()
// {
//     eci2kep(eci, kep);
//     buttonUpdateKepOrbit();
// }

void MainWindow::exportPlots()
{
    FILE *fp = fopen("plot_data.txt", "w");
    vector < vector < double >> data;
    vector < string > names;

    for (size_t i=0; i<widgetHandler.size(); ++i)
    {
        if (widgetHandler.getType(i) == CosmosWidgetHandler::widgetType::PLOT)
        {
            CosmosPlot *tplot = (CosmosPlot *)widgetHandler.getWidget(i);
            if (data.empty())
            {
                data.push_back(tplot->stackX);
                names.push_back("UTC");
            }
            for (size_t k=0; k<tplot->stackY.size(); ++k)
            {
                data.push_back(tplot->stackY[k]);
                names.push_back(tplot->getCosmosName());
            }
        }
    }
    for (size_t i=0; i<names.size(); ++i)
    {
        fprintf(fp, "%s\t",names[i].c_str());
    }
    fprintf(fp, "\n");
    for (size_t j=0; j<data[0].size(); ++j)
    {
        for (size_t i=0; i<data.size(); ++i)
        {
            fprintf(fp, "%g\t", data[i][j]);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
}

void MainWindow::exporteci(){
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), QString(),
                tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

        if (!fileName.isEmpty()) {
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly)) {
                // error message
            } else {
                QTextStream stream(&file);
                stream << "Xpos: " << eci.s.col[0] << endl;
                stream << "Ypos: " << eci.s.col[1] << endl;
                stream << "Zpos: " << eci.s.col[2] << endl;
                stream << "Xvel: " << eci.v.col[0] << endl;
                stream << "Yvel: " << eci.v.col[1] << endl;
                stream << "Zvel: " << eci.v.col[2] << endl;
                stream << "Xaccel: " << eci.a.col[0] << endl;
                stream << "Yaccel: " << eci.a.col[0] << endl;
                stream << "Zaccel: " << eci.a.col[0] << endl;


                stream.flush();
                file.close();
            }
        }
}


void MainWindow::exportkep(){

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), QString(),
                tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

        if (!fileName.isEmpty()) {
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly)) {
                // error message
            } else {
                QTextStream stream(&file);
                stream << "time: " << kep.utc <<endl;
                stream << "orbit number: " << kep.orbit << endl;
                stream << "orbital period: " << kep.period << endl;
                stream << "semi-major axis: " << kep.a << endl;
                stream << "eccentricity: " << kep.e << endl;
                //stream << "angular momentum: " << kep.h.col[0] <<endl;
                stream << "                  " << kep.h.col[1] << endl;
                stream << "                  " << kep.h.col[2] <<endl;
                stream << "solar beta angle (rad): " << kep.beta << endl;
                stream << "orbital inclination (rad): " << kep.i << endl;
                stream << "right acension of the ascending node (rad): " << kep.raan << endl;
                stream << "argument of perigee: " << kep.ap << endl;
                stream << "argument of latitude: " << kep.alat << endl;
                stream << "mean anomoly: " << kep.ma << endl;
                stream << "true anomoly: " << kep.ta << endl;
                stream << "eccentric anomoly: " << kep.ea << endl;
                stream << "mean motion: " << kep.mm << endl;
                stream.flush();
                file.close();
            }
        }
}

void MainWindow::importData()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(),
            tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly)){
            QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
            return;
        }else{
            QTextStream in(&file);
            while (!in.atEnd())
            {
                QString line = in.readLine();
                std::cout << line.toStdString();
                //TODO parse file for kep elements and set them to the values in kepstruc

            }
               file.close();
        }
    }
}

void MainWindow::setPayloadsState(cosmosstruc *cinfo)
{
    for (size_t i=0; i<widgetHandler.size(); ++i)
    {
        if (widgetHandler.getType(i) == CosmosWidgetHandler::widgetType::PAYLOADS)
        {
            CosmosPayloads *tpayload = (CosmosPayloads *)widgetHandler.getWidget(i);
            for (size_t j=0; j<tpayload->getTabstrucCount(); ++j)
            {
                CosmosPayloads::tabstruc *ttab = tpayload->getTabstruc(j);
                cinfo->device[ttab->cidx].all.flag &= ~DEVICE_FLAG_ON;
                for (size_t k=0; k<ttab->conditions.size(); ++k)
                {
                    const char *ptr = (char *)ttab->conditions[k].condition.c_str();
                    if (json_equation(ptr, cinfo))
                    {
                        cinfo->device[ttab->cidx].all.flag |= DEVICE_FLAG_ON;
                    }
                }
            }
        }
    }
}

bool fileExist(string fileName)
{
    ifstream infile(fileName);
    return infile.good();
}

void MainWindow::createMenus()
{

    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(importAct);
    fileMenu->addAction(exportEciAct);
    fileMenu->addAction(exportKepAct);
    fileMenu->addAction(exportPlotsAct);
    fileMenu->addAction(exitAct);

    viewMenu = menuBar()->addMenu(tr("&View"));
    viewMenu->addAction(plotsAct);
    viewMenu->addAction(posAct);
    viewMenu->addAction(payAct);
}

void MainWindow::createActions()
{

    newAct = new QAction(tr("&New Window"));
    newAct->setStatusTip(tr("Create a New Window"));
    connect(newAct, &QAction::triggered, [&]() { MainWindow *window = new MainWindow; window->show();});

    importAct = new QAction(tr("&Import"), this);
    importAct->setStatusTip(tr("Import Parameters"));
    connect(importAct, &QAction::triggered, this, &MainWindow::importData);

    exportEciAct = new QAction(tr("&Export ECI Parameters"));
    exportEciAct->setStatusTip(tr("Export the ECI Orbit Parameters"));
    connect(exportEciAct, &QAction::triggered, this, &MainWindow::exporteci);

    exportKepAct = new QAction(tr("Export &KEP Parameters"));
    exportKepAct->setStatusTip(tr("Export the KEP Orbit Parameters"));
    connect(exportKepAct, &QAction::triggered, this, &MainWindow::exportkep);

    exportPlotsAct = new QAction(tr("Export &Plots"));
    exportPlotsAct->setStatusTip(tr("Export Plot Data"));
    connect(exportPlotsAct, &QAction::triggered, this, &MainWindow::exportPlots);

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, &QAction::triggered, this, &QWidget::close);

    plotsAct = new QAction(tr("P&lots"));
    plotsAct->setCheckable(true);
    plotsAct->setChecked(true);
    plotsAct->setStatusTip(tr("Toggle On and Off the Plots"));
    connect(plotsAct, &QAction::toggled, this, &MainWindow::trigPlotVis);

    posAct = new QAction(tr("P&osition"));
    posAct->setCheckable(true);
    posAct->setChecked(true);
    posAct->setStatusTip(tr("Toggle On and Off the Position"));
    connect(posAct, &QAction::toggled, this, &MainWindow::trigPosVis);

    payAct = new QAction(tr("Pay&loads"));
    payAct->setCheckable(true);
    payAct->setChecked(true);
    payAct->setStatusTip(tr("Toggle On and Off the Payloads"));
    connect(payAct, &QAction::toggled, this, &MainWindow::trigPayVis);
}
