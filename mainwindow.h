#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "support/configCosmos.h" // must be included here before everything else otherwise we get strange errors
#include "cosmoswidget/cosmoswidgethandler.h"
#include "cosmoswidget/cosmoslabel.h"
#include "cosmoswidget/cosmosspinbox.h"
#include "cosmoswidget/cosmosdoublespinbox.h"
#include "cosmoswidget/cosmosplot.h"
#include "cosmoswidget/cosmospayloads.h"
#include "cosmoswidget/cosmoselements.h"
#include "cosmoswidget/cosmosmap.h"
#include "agent/agentclass.h"
#include "support/convertlib.h"
#include "physics/physicslib.h"
#include "support/jsonlib.h"

// Qt libs
#include <QMainWindow>
#include <QSizePolicy>
#include <QSpacerItem>
#include <QProcess>
#include <QTimer>
#include <QFile>
#include <QDir>

// for UI
#include <QComboBox>
#include <QRadioButton>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QAction>
#include <QToolTip>
#include <QFileDialog>
#include <QMessageBox>

//for menus
#include <QAction>
#include <QMenu>
#include <QMenuBar>

void cthreadloop();

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

    std::thread cthread;

signals:

    void plotVis(bool);
    void posVis(bool);
    void payVis(bool);

public slots:

protected slots:


private slots:
    void comboSelectNode(int item);
    void buttonGoToStart();
//    void buttonStepBackward();
//    void buttonPlayBackward();
//    void buttonPause();
//    void buttonPlayForward();
//    void buttonStepForward();
//    void buttonGoToEnd();
    void sliderIter(int newDataIter);

    void buttonUpdateKepOrbit();
//    void buttonUpdateEciOrbit();
//    void spinEccentricity();
//    void spinAltitude(QWidget *widget);
//    void spinInclination();
//    void spinRaan();
//    void spinAp();


    void timeDataUpdate();
    void exportPlots();
    void setPayloadsState(cosmosstruc *cinfo);
    void exportkep();
    void exporteci();
    void importData();

    void trigPlotVis(bool toggle) {emit plotVis(toggle);}
    void trigPosVis(bool toggle) {emit posVis(toggle);}
    void trigPayVis(bool toggle) {emit payVis(toggle);}

private:

    QTimer *timeDataTimer;

    // Data items
    CosmosWidgetHandler widgetHandler;
    CosmosLabel *labelNodeLocPos;
    CosmosDoubleSpinBox *spinNodeUtc;
    CosmosSpinBox *dataiteration;
    CosmosPlot *plotNodeLocPos;

    // Control items
    QComboBox *m_comboSelectNode;
    //QComboBox *m_comboSelectElements; //added to make a new combobox
    CosmosPushButton *m_buttonGoToStart;
    CosmosPushButton *m_buttonGoToEnd;
    CosmosPushButton *m_buttonPause;
    CosmosPushButton *m_buttonStepBackward;
    CosmosPushButton *m_buttonStepForward;
    CosmosPushButton *m_buttonPlayBackward;
    CosmosPushButton *m_buttonPlayForward;
//    CosmosPushButton *m_buttonExportPlots;
//    CosmosSlider *m_sliderIter;
    QSlider *m_sliderIter;

    size_t timeDataIter = SIZE_MAX;
    size_t timeDataIterHold = SIZE_MAX;
    int32_t timeDataDirection = 0;
    double timeDataValue = 0.;
    double timeDataSpeed = 1.;
    double timeDataOffset = 0.;
    double timeDataBase = 0.;
    ElapsedTime timeDataEt;

    Agent cagent;
//    cosmosstruc *cdata;
    jsonnode json;
	cosmosinfostack timeInfoStack;
    cosmosstruc *timeInfoPtr = nullptr;
//    cosmosdatastruc *timeInfoPtr = nullptr;
//    cosmosmetastruc *timeMetaPtr = nullptr;

    kepstruc kep;
    cartpos eci;

	string sohstring;

    vector < cosmosstruc *> cinfos;

//menu related
    void createActions();
    void createMenus();

    QMenu *fileMenu;
    QMenu *viewMenu;

    QAction *newAct;
    QAction *exitAct;
    QAction *importAct;
    QAction *exportEciAct;
    QAction *exportKepAct;
    QAction *exportPlotsAct;

    QAction *plotsAct;
    QAction *posAct;
    QAction *payAct;

};

#endif // MAINWINDOW_H
